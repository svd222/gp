# INSTALLATION #

$ sudo mkdir -m 0755 /var/www/gp && cd gp

$ git clone git@bitbucket.org:svd222/gp.git .

$ composer install

## HOW TO USE ##

$ /path/to/php /var/www/gp/index.php 

or

$ php /var/www/gp/index.php (if path to php in a PATH variable)

type array elements divided by commas 

## RUN TESTS ##

$ vendor/bin/phpunit --bootstrap vendor/autoload.php tests/unit