<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 01.02.19
 * Time: 12:45
 */
use PHPUnit\Framework\TestCase;
use svd\gp\GeometricalProgression;

final class ProgressionTest extends TestCase
{
    protected $infinityIncreasingData = [2, 4, 8, 16, 32];

    protected $infinityDecreasingData = [100, 50, 25, 12.5, 6.25];

    protected $alternatingData = [3, -6, 12, -24, 48];

    protected $invalidData = [2, 4, 8, 16, 47];

    protected $invalidDataWithFirstItemIs0 = [0, 2, 4, 8, 16];

    protected $invalidDataWithItemIs0 = [1, 2, 0, 8, 16];

    protected $negativeValues = [-3, -6, -12, -24, -48];

    /**
     * @var GeometricalProgression
     */
    protected $infinityIncreasingProgression;

    /**
     * @var GeometricalProgression
     */
    protected $infinityDecreasingProgression;

    /**
     * @var GeometricalProgression
     */
    protected $alternatingProgression;

    /**
     * @var GeometricalProgression
     */
    protected $invalidProgression;

    /**
     * @var GeometricalProgression
     */
    protected $negativeValuesProgression;

    public function setUp()
    {
        $this->infinityIncreasingProgression = new GeometricalProgression($this->infinityIncreasingData);
        $this->infinityDecreasingProgression = new GeometricalProgression($this->infinityDecreasingData);
        $this->alternatingProgression = new GeometricalProgression($this->alternatingData);
        $this->invalidProgression = new GeometricalProgression($this->invalidData);
        $this->negativeValuesProgression = new GeometricalProgression($this->negativeValues);
    }

    public function testIsValid()
    {
        $this->assertTrue($this->infinityIncreasingProgression->getIsValid());
    }

    /**
     * @depends testIsValid
     */
    public function testGetParamsInfinityIncreasingProgression()
    {
        $this->assertEquals([
            'firstItem' => 2,
            'isValid' => true,
            'isInfinityDecreasing' => false,
            'isInfinityIncreasing' => true,
            'isAlternatingProgression' => false,
            'denominator' => 2
        ], $this->infinityIncreasingProgression->getParams());
    }

    /**
     * @depends testIsValid
     */
    public function testGetParamsInfinityDecreasingProgression()
    {
        $this->assertEquals([
            'firstItem' => 100,
            'isValid' => true,
            'isInfinityDecreasing' => true,
            'isInfinityIncreasing' => false,
            'isAlternatingProgression' => false,
            'denominator' => 0.5
        ], $this->infinityDecreasingProgression->getParams());
    }

    /**
     * @depends testIsValid
     */
    public function testGetParamsAlternateProgression()
    {
        $this->assertEquals([
            'firstItem' => 3,
            'isValid' => true,
            'isInfinityDecreasing' => false,
            'isInfinityIncreasing' => false,
            'isAlternatingProgression' => true,
            'denominator' => -2
        ], $this->alternatingProgression->getParams());
    }

    public function testGetParamsNegativeValuesProgression()
    {
        $this->assertEquals([
            'firstItem' => -3,
            'isValid' => true,
            'isInfinityDecreasing' => false,
            'isInfinityIncreasing' => true,
            'isAlternatingProgression' => false,
            'denominator' => 2
        ], $this->negativeValuesProgression->getParams());
    }

    /**
     * @depends testIsValid
     */
    public function testGetItemIncreasingProgression()
    {
        $item = $this->infinityIncreasingProgression
            ->getItem(4);
        $this->assertEquals(32, $item);
    }

    /**
     * @depends testIsValid
     */
    public function testGetItemDecreasingProgression()
    {
        $item = $this->infinityDecreasingProgression
            ->getItem(4);
        $this->assertEquals(6.25, $item);
    }

    /**
     * @depends testIsValid
     */
    public function testGetItemAlternatingProgression()
    {
        $item = $this->alternatingProgression
            ->getItem(4);
        $this->assertEquals(48, $item);
    }

    /**
     * @depends testIsValid
     */
    public function testGetSummIncreasingProgression()
    {
        $summ = $this->infinityIncreasingProgression
            ->getSum(4);
        $this->assertEquals(62, $summ);
    }

    /**
     * @depends testIsValid
     */
    public function testGetSummAlternatingProgression()
    {
        $summ = $this->alternatingProgression
            ->getSum(4);
        $this->assertEquals(33, $summ);
    }

    /**
     * @depends testIsValid
     */
    public function testGetSummNegativeValuesProgression()
    {
        $summ = $this->negativeValuesProgression
            ->getSum(4);
        $this->assertEquals(-93, $summ);
    }

    public function testIsInvalid()
    {
        $this->assertFalse($this->invalidProgression->getIsValid());
    }

    public function testWhenInputDataIsTooSmall()
    {
        $this->expectException(\InvalidArgumentException::class);
        $progression = new GeometricalProgression([3]);
    }

    public function testInvalidProgressionWithFirstItemIs0()
    {
        $this->expectException(\InvalidArgumentException::class);
        $progression = new GeometricalProgression($this->invalidDataWithFirstItemIs0);
    }

    /**
     * @depends testInvalidProgressionWithFirstItemIs0
     */
    public function testInvalidProgressionWithItemIs0()
    {
        $progression = new GeometricalProgression($this->invalidDataWithItemIs0);
        $this->assertFalse($progression->getIsValid());
    }

    /**
     * @depends testIsInvalid
     */
    public function testGetItemResultIsNullWhenProgressionIsInvalid()
    {
        $item = $this->invalidProgression->getItem(3);
        $this->assertNull($item);
    }

    /**
     * @depends testGetItemResultIsNullWhenProgressionIsInvalid
     */
    public function testGetSummResultIsNullWhenProgressionIsInvalid()
    {
        $summ = $this->invalidProgression->getSum(3);
        $this->assertNull($summ);
    }
}