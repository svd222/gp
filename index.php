<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 01.02.19
 * Time: 12:58
 */
require __DIR__ . '/vendor/autoload.php';

use svd\gp\GeometricalProgression;

echo "Please type array divided by commas\n";
$f = fopen('php://stdin', 'r');
$data = fgets($f);
fclose($f);
var_dump($data);
$data = explode(',', $data);
$data = array_map("floatval", array_map("trim", $data));

$gp = new GeometricalProgression($data);
if ($gp->getIsValid()) {
    $params = $gp->getParams();
    echo "Incoming data is a geometrical progression\n\n";
    echo "Params:\n\n";
    echo "First item: " . $params['firstItem'] . "\n";
    echo "Denominator: " . $params['denominator'] . "\n";
    echo "Is infinity decreasing: " . (($params['isInfinityDecreasing']) ? "yes": "no") . "\n";
    echo "Is infinity increasing: " . (($params['isInfinityIncreasing']) ? "yes": "no") . "\n";
    echo "Is alternate: " . (($params['isAlternatingProgression']) ? "yes": "no") . "\n";
} else {
    echo "This set of numbers is not a geometrical progression";
}
