<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 01.02.19
 * Time: 13:25
 */
namespace svd\gp;

class GeometricalProgression
{
    /**
     * @var array
     */
    private $progression;

    /**
     * @var int
     */
    private $firstItem;

    /**
     * @var bool
     */
    private $isValid = false;

    /**
     * @var bool
     */
    private $isInfinityDecreasing = false;

    /**
     * @var bool
     */
    private $isInfinityIncreasing = false;

    /**
     * @var bool
     */
    private $isAlternatingProgression = false;

    /**
     * @var int
     */
    private $denominator;

    public function __construct(array $progression)
    {
        $this->calculateParams($progression);
    }

    /**
     * Calculate params
     *
     * @return bool
     */
    private function calculateParams(array $data)
    {
        $count = count($data);
        $isValid = true;
        if ($data[0] == 0) {
            throw new \InvalidArgumentException('The first element cannot be 0');
        }
        if ($count > 1) {
            $denominator = $data[1]/$data[0];
            for ($i = 1; $i < $count - 1; $i++) {
                if ($data[$i] == 0) {
                    $isValid = false;
                    break;
                }
                if ($data[$i] * $denominator != $data[$i + 1]) {
                    $isValid = false;
                    break;
                }
            }

            $this->denominator = $denominator;
            $this->firstItem = $data[0];
            $this->isValid = $isValid;
            $this->progression = $data;

            if ($denominator < 0) {
                $this->isAlternatingProgression = true;
            } else {
                if ($denominator > 1) {
                    $this->isInfinityIncreasing = true;
                } else {
                    if ($denominator > 0 && $denominator < 1)
                    $this->isInfinityDecreasing = true;
                }
            }
        } else {
            throw new \InvalidArgumentException('Input data should have at least two elements');
        }
    }

    /**
     * Returns progression params
     *
     * @return array [
     *  'firstItem' => int,
     *  'isValid' => bool,
     *  'isInfinityDecreasing' => bool,
     *  'isInfinityIncreasing' => bool,
     *  'isAlternatingProgression' => bool,
     *  'denominator' => int
     * ]
     */
    public function getParams()
    {
        return [
            'firstItem' => $this->firstItem,
            'isValid' => $this->isValid,
            'isInfinityDecreasing' => $this->isInfinityDecreasing,
            'isInfinityIncreasing' => $this->isInfinityIncreasing,
            'isAlternatingProgression' => $this->isAlternatingProgression,
            'denominator' => $this->denominator
        ];
    }

    /**
     * Returns progression
     *
     * @return array
     */
    public function getProgression()
    {
        return $this->progression;
    }

    /**
     * Calculates the summ of progression if it valid
     *
     * @param $index
     * @return float|int
     */
    public function getSum($index)
    {
        $summ = null;
        if ($this->isValid) {
            if ($this->isInfinityIncreasing || $this->isAlternatingProgression) {
                if ($this->denominator != 1) {
                    $summ = ($this->firstItem - ($this->getItem($index) * $this->denominator))/(1 - $this->denominator);
                } else {
                    $summ = $index * $this->firstItem;
                }
            } else {
                if ($this->isInfinityDecreasing) {
                    $summ = $this->firstItem/(1 - $this->denominator);
                }
            }
        }
        return $summ;
    }

    /**
     * Gets the item of progression by index
     *
     * @param $index
     * @return int
     */
    public function getItem($index)
    {
        if ($this->isValid) {
            /**
             * Right formula is $this->firstItem * pow($this->denominator, $index - 1);
             * But in php counting starts from zero not from one.
             */
            return $this->firstItem * pow($this->denominator, $index);
        }
        return null;
    }

    /**
     * Returns true if progression is valid geometrical progression, false otherwise
     *
     * @return bool
     */
    public function getIsValid()
    {
        return $this->isValid;
    }
}